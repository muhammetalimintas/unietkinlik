package com.example.muhammet.unietkinlik.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammet.unietkinlik.Constants;
import com.example.muhammet.unietkinlik.EtkinlikAdapter;
import com.example.muhammet.unietkinlik.R;
import com.example.muhammet.unietkinlik.RequestInterface;
import com.example.muhammet.unietkinlik.fragments.ProfileFragment;
import com.example.muhammet.unietkinlik.models.EtkModel;
import com.example.muhammet.unietkinlik.models.Etkinlik;
import com.example.muhammet.unietkinlik.models.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ActivityScreen extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private static ListView listView;

    Spinner spinner;
    private static ProgressDialog pDialog;
    public Resources res;
    TextView baslik, tarih, tv_lat, tv_long;

    public String fromBOUN = "get_all_BOUN";
    public String fromMED = "get_all_MED";
    public String fromITU = "get_all_ITU";
    public String fromYTU = "get_all_YTU";
    public String fromMU = "get_all_MU";
    public String tarihe_gore = "a";
    public String bugun = "bugun";

    public double latitude = 40.9984;
    public double longitude = 28.9096;

    public EtkinlikAdapter adapter;
    public static TextView editLat, editLong;
    ProfileFragment profile;
    private ArrayList<EtkModel.EtkModelData> data, data2, data3;


    String spinnerValue = "";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);

        pDialog = new ProgressDialog(ActivityScreen.this);
        pDialog.setMessage("Lütfen Bekleyiniz...");
        pDialog.setCancelable(false);

        spinner = (Spinner) findViewById(R.id.act_main_etkinlikSpinner);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.options, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        baslik = (TextView) findViewById(R.id.baslik);
        tarih = (TextView) findViewById(R.id.tv_tarih);



        //tarih.setOnClickListener();

        listView = (ListView) findViewById(R.id.act_main_etkinlikLV);

        DateFormat df = new SimpleDateFormat("dd MMMM yyyy", new Locale("tr"));
        tarih.setText(df.format(new Date()));


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

       /*
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access

            builder.setTitle("UYARI")
                    .setMessage("Uygulama cihazınızın konumuna erişmek istiyor. İzin vermek için Konum Ayarları'na tıklayın.")
                    .setPositiveButton("Konum Ayarları", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //SimpleLocation.openSettings(ActivityScreen.this);

                        }
                    })
                    .setNegativeButton("İptal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
            AlertDialog alert2 = builder.create();
            alert2.show();
        }
*/


        builder.setTitle("UYARI")
                .setMessage("Yıldız Teknik Üniversitesi'ne ait etkinliklerde görülen tarihler " +
                        "etkinliklerin sisteme eklendiği tarihler olabilir. Ayrıntılı bilgiye " +
                        "etkinlik adına tıklayarak ulaşabilirsiniz. ")
                .setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert1 = builder.create();
        alert1.show();
        profile = new ProfileFragment();
        //editLat.setText("40.9984");
        //editLong.setText("28.9096");


        baslik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ActivityScreen.this.onItemSelected();
                spinnerValue = spinner.getSelectedItem().toString();
                //Toast.makeText(ActivityScreen.this, spinnerValue.toString(), Toast.LENGTH_SHORT).show();


                switch (spinnerValue.toString()) {
                    case "Tarihe Göre(Yakın)":
                        begeniKontrolEt(profile.user_email.toString(), tarihe_gore);
                        break;

                    // case "Konuma Göre":
                    //getJsonByLocation(editLat.getText().toString(),editLong.getText().toString());
                    //   break;

                    case "Boğaziçi Üniversitesi":
                        begeniKontrolEt(profile.user_email.toString(), fromBOUN);
                        break;
                    case "İstanbul Medeniyet Üniversitesi":
                        begeniKontrolEt(profile.user_email.toString(), fromMED);
                        break;
                    case "İstanbul Teknik Üniversitesi":
                        begeniKontrolEt(profile.user_email.toString(), fromITU);
                        break;
                    case "Yıldız Teknik Üniversitesi":
                        begeniKontrolEt(profile.user_email.toString(), fromYTU);
                        break;
                    case "Marmara Üniversitesi":
                        begeniKontrolEt(profile.user_email.toString(), fromMU);
                        break;
                    case "SIRALA":
                        begeniKontrolEt(profile.user_email.toString(), tarihe_gore);
                        break;
                    case "Sadece Bugün":
                        begeniKontrolEt(profile.user_email.toString(), bugun);
                        break;
                }

            }
        });


        //new GetActivities(tarihe_gore).execute();
        //getJsonRetrofitArray(tarihe_gore);
        begeniKontrolEt(profile.user_email.toString(), tarihe_gore);
        //katilimKontrolEt(data2,profile.user_email.toString(),tarihe_gore);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {

        switch (pos) {
            case 0: //sirala
                break;

            case 1: // tarihe göre
                begeniKontrolEt(profile.user_email.toString(), tarihe_gore);
                break;
            case 2: //sadece bugun olanlar
                begeniKontrolEt(profile.user_email.toString(), bugun);
                break;
            /*
            case 3: //konuma göre
                //getJsonByLocation(editLat.getText().toString(),editLong.getText().toString());
                break;
                */
            case 3: //bogazici
                begeniKontrolEt(profile.user_email.toString(), fromBOUN);
                break;
            case 4: //medeniyet
                begeniKontrolEt(profile.user_email.toString(), fromMED);
                break;
            case 5: //itu
                begeniKontrolEt(profile.user_email.toString(), fromITU);
                break;
            case 6: //ytu
                begeniKontrolEt(profile.user_email.toString(), fromYTU);
                break;
            case 7: //marmara
                begeniKontrolEt(profile.user_email.toString(), fromMU);
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void katil(String etk_id, String user_email, String etk_katilimci_sayisi) {
        showpDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //Toast.makeText(ActivityScreen.this, s.toString(), Toast.LENGTH_SHORT).show();
        RequestInterface service = retrofit.create(RequestInterface.class);

        User user = new User();
        Etkinlik etk = new Etkinlik();
        etk.setEtk_id(etk_id.toString());
        etk.setEtk_katilimci_sayisi(etk_katilimci_sayisi.toString());
        user.setEmail(user_email.toString());

        Call<Etkinlik> call = service.yeniKatilim(etk.getEtk_id(), user.getEmail(), etk.getEtk_katilimci_sayisi());
        call.enqueue(new Callback<Etkinlik>() {
            @Override
            public void onResponse(Call<Etkinlik> call, Response<Etkinlik> response) {
                hidepDialog();
                Log.d("onResponse", "" + response.code() +
                        "  response body " + response.body() +
                        " responseError " + response.errorBody() + " responseMessage " +
                        response.message());
            }

            @Override
            public void onFailure(Call<Etkinlik> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void likeGuncelle(String etk_id, String user_email, String etk_like) {
        showpDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //Toast.makeText(ActivityScreen.this, s.toString(), Toast.LENGTH_SHORT).show();
        RequestInterface service = retrofit.create(RequestInterface.class);
        Etkinlik etk = new Etkinlik();
        User user = new User();
        etk.setEtk_id(etk_id.toString());
        etk.setEtk_like(etk_like.toString());
        user.setEmail(user_email.toString());

        Call<Etkinlik> call = service.likeArttir(etk.getEtk_id(), user.getEmail(), etk.getEtk_like());
        call.enqueue(new Callback<Etkinlik>() {
            @Override
            public void onResponse(Call<Etkinlik> call, Response<Etkinlik> response) {
                hidepDialog();
                Log.d("onResponse", "" + response.code() +
                        "  response body " + response.body() +
                        " responseError " + response.errorBody() + " responseMessage " +
                        response.message());

            }

            @Override
            public void onFailure(Call<Etkinlik> call, Throwable t) {
                hidepDialog();
                Log.d("onFailure", t.toString());
            }
        });

    }

//önce begenileri al
    //begeniyi aldığın fonksiyonda doğru çalışırsa ektinlkleri al


    public void begeniKontrolEt(final String email, final String choose) {
        showpDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //Toast.makeText(ActivityScreen.this, s.toString(), Toast.LENGTH_SHORT).show();
        RequestInterface service = retrofit.create(RequestInterface.class);

        User user = new User();
        user.setEmail(email.toString());
        Call<EtkModel> call = service.checkLikeStatus(user.getEmail());
        call.enqueue(new Callback<EtkModel>() {
            @Override
            public void onResponse(Call<EtkModel> call, Response<EtkModel> response) {

                EtkModel jsonResponse = response.body();
                data2 = new ArrayList<>(Arrays.asList(jsonResponse.getData()));

                Log.d("onResponse", email + ", " + data2.get(0).getEtk_id() + " id'li etkinligi begenmis");
                katilimKontrolEt(data2, email, choose);

            }

            @Override
            public void onFailure(Call<EtkModel> call, Throwable t) {
                Log.d("onFailure", email + ", " + data2.get(0).getEtk_id() + " id'li etkinligi begenmemis   " + t.getMessage());
            }
        });
    }

    public void katilimKontrolEt(final ArrayList<EtkModel.EtkModelData> data2, final String email, final String choose) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //Toast.makeText(ActivityScreen.this, s.toString(), Toast.LENGTH_SHORT).show();
        RequestInterface service = retrofit.create(RequestInterface.class);

        User user = new User();
        user.setEmail(email.toString());
        Call<EtkModel> call = service.checkKatilimStatus(user.getEmail());
        call.enqueue(new Callback<EtkModel>() {
            @Override
            public void onResponse(Call<EtkModel> call, Response<EtkModel> response) {

                //Log.d("",response.body().getEtk_adi());

                EtkModel jsonResponse = response.body();
                data3 = new ArrayList<>(Arrays.asList(jsonResponse.getData()));

                Log.d("onResponse", email + ", " + data3.get(0).getEtk_id() + " id'li etkinlige katılıyor    " + response.isSuccessful());

                getJsonRetrofitArray(data2, data3, choose);


            }

            @Override
            public void onFailure(Call<EtkModel> call, Throwable t) {
                Log.d("onFailure", email + ", " + data3.get(0).getEtk_id() + " id'li etkinlige katilmiyor    " + t.getMessage());
            }
        });
    }

    private void getJsonRetrofitArray(final ArrayList<EtkModel.EtkModelData> data2, final ArrayList<EtkModel.EtkModelData> data3, String choose) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL).//ip
                addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface service = retrofit.create(RequestInterface.class);
        Call<EtkModel> modelCall = service.getActivityDetails(choose);

        modelCall.enqueue(new Callback<EtkModel>() {
            @Override
            public void onResponse(Call<EtkModel> call, Response<EtkModel> response) {
                hidepDialog();
                try {
                    //EtkModel model  = response.body();
                    EtkModel jsonResponse = response.body();
                    data = new ArrayList<>(Arrays.asList(jsonResponse.getData()));

                    //EtkModel.EtkModelData[] userList  = model.getData();
                    //for işlemini burda yapılacak

                    if (data == null) {
                        Toast.makeText(ActivityScreen.this, "Kayıtı Veri Yok", Toast.LENGTH_SHORT).show();
                    } else {
                        if (data2 != null) {
                            for (int i = 0; i < data2.size(); i++) {
                                for (int j = 0; j < data.size(); j++) {
                                    if (data2.get(i).getEtk_id().equals(data.get(j).getEtk_id())) {
                                        data.get(j).setBegeniDurumu("Beğendin");
                                    } else {
                                        data.get(j).setBegeniDurumu("");
                                    }
                                }
                            }
                        }
                        if (data3 != null) {
                            for (int i = 0; i < data3.size(); i++) {
                                for (int j = 0; j < data.size(); j++) {
                                    if (data3.get(i).getEtk_id().equals(data.get(j).getEtk_id())) {
                                        data.get(j).setKatilimDurumu("KATILMAKTAN VAZGEÇ");
                                    } else {
                                        data.get(j).setKatilimDurumu("KATIL");
                                    }
                                }
                            }
                        }
                        adapter = new EtkinlikAdapter(ActivityScreen.this, data);
                        listView.setAdapter(adapter);
                        ////adapter.notifyDataSetChanged();
                        //Toast.makeText(ActivityScreen.this, "SIKINTI YOK", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    Toast.makeText(ActivityScreen.this, "Bu seçenek için veritabanında kayıt bulunamadı. Tarihe göre sıralanıyor. ", Toast.LENGTH_LONG).show();
                    begeniKontrolEt(profile.user_email.toString(), tarihe_gore);
                }

            }

            @Override
            public void onFailure(Call<EtkModel> call, Throwable t) {
                hidepDialog();
                Toast.makeText(ActivityScreen.this, "Lütfen Daha Sonra Tekrar Deneyiniz" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

    }
}

