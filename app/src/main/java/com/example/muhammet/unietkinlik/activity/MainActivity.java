package com.example.muhammet.unietkinlik.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.muhammet.unietkinlik.Constants;
import com.example.muhammet.unietkinlik.R;
import com.example.muhammet.unietkinlik.fragments.LoginFragment;
import com.example.muhammet.unietkinlik.fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity {

    public SharedPreferences pref;
    public static Context contextOfApplication;
    //public SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = getPreferences(0);
        //editor = pref.edit();
        contextOfApplication = getApplicationContext();

        if(!isNetworkAvailable(this)) {
            Toast.makeText(this,"Internet bağlantınızı açın ve uygulamayı tekrar başlatın",Toast.LENGTH_LONG).show();
            finish(); //Calling this method to close this activity when internet is not available.
        }

        initFragment();

    }
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(conMan.getActiveNetworkInfo() != null && conMan.getActiveNetworkInfo().isConnected())
            return true;
        else
            return false;
    }
    private void initFragment(){
        Fragment fragment;
        if(pref.getBoolean(Constants.IS_LOGGED_IN,false)){
            fragment = new ProfileFragment();
        }else {
            fragment = new LoginFragment();
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame,fragment);
        ft.commit();
    }

}
