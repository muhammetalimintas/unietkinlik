package com.example.muhammet.unietkinlik.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by muhammet on 30.05.2017.
 */

public class Etkinlik {

    @SerializedName("etkinlik")
    List<Etkinlik> etkinlikler;

    public List<Etkinlik> getEtkinlikler() {
        return etkinlikler;
    }
    @SerializedName("etk_id")
    private String etk_id;
    @SerializedName("etk_adi")
    private String etk_adi;
    @SerializedName("etk_tarih_saat")
    private String etk_tarih_saat;
    @SerializedName("etk_katilimci_sayisi")
    private String etk_katilimci_sayisi;
    @SerializedName("etk_like")
    private String etk_like;

    @SerializedName("etk_link")
    private String etk_link;

    @SerializedName("etk_uni")
    private List<Uni> etk_uni;
    /*
        public String etk_uni;

        public String etk_lat;

        public String etk_long;

    */
    public String getEtk_id() {
        return etk_id;
    }

    public void setEtk_id(String etk_id) {
        this.etk_id = etk_id;
    }

    public String getEtk_adi() {
        return etk_adi;
    }

    public void setEtk_adi(String etk_adi) {
        this.etk_adi = etk_adi;
    }

    public String getEtk_tarih_saat() {
        return etk_tarih_saat;
    }

    public void setEtk_tarih_saat(String etk_tarih) {
        this.etk_tarih_saat = etk_tarih;
    }



    public String getEtk_katilimci_sayisi() {
        return etk_katilimci_sayisi;
    }

    public void setEtk_katilimci_sayisi(String etk_katilimci_sayisi) {
        this.etk_katilimci_sayisi = etk_katilimci_sayisi;
    }

    public String getEtk_like() {
        return etk_like;
    }

    public void setEtk_like(String etk_like) {
        this.etk_like = etk_like;
    }


    public String getEtk_link() {
        return etk_link;
    }

    public void setEtk_link(String etk_link) {
        this.etk_link = etk_link;
    }


    public List<Uni> getEtk_uni() {
        return etk_uni;
    }


    public void setEtk_uni(List<Uni> etk_uni) {
        this.etk_uni = etk_uni;
    }

/*
    public String getEtk_uni() {
        return etk_uni;
    }

    public void setEtk_uni(String etk_uni) {
        this.etk_uni = etk_uni;
    }

    public String getEtk_lat() {
        return etk_lat;
    }

    public void setEtk_lat(String etk_lat) {
        this.etk_lat = etk_lat;
    }

    public String getEtk_long() {
        return etk_long;
    }

    public void setEtk_long(String etk_long) {
        this.etk_long = etk_long;
    }
*/

    public static class Uni{
        @SerializedName("uni_id")
        public String etk_uni_id;
        @SerializedName("uni_adi")
        public String etk_uni;
        @SerializedName("latitude")
        public String etk_lat;
        @SerializedName("longitude")
        public String etk_long;

        public String getEtk_uni_id() {
            return etk_uni_id;
        }

        public void setEtk_uni_id(String etk_uni_id) {
            this.etk_uni_id = etk_uni_id;
        }

        public String getEtk_uni() {
            return etk_uni;
        }

        public void setEtk_uni(String etk_uni) {
            this.etk_uni = etk_uni;
        }

        public String getEtk_lat() {
            return etk_lat;
        }

        public void setEtk_lat(String etk_lat) {
            this.etk_lat = etk_lat;
        }

        public String getEtk_long() {
            return etk_long;
        }

        public void setEtk_long(String etk_long) {
            this.etk_long = etk_long;
        }
    }

}
