package com.example.muhammet.unietkinlik;


import com.example.muhammet.unietkinlik.models.EtkModel;
import com.example.muhammet.unietkinlik.models.Etkinlik;
import com.example.muhammet.unietkinlik.models.ServerRequest;
import com.example.muhammet.unietkinlik.models.ServerResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RequestInterface {

    //register, login islemleri icin kullanilir
    @POST("uni_etkinlik/")
    Call<ServerResponse> operation(@Body ServerRequest request);

    //etkinlikleri cekmek icin kullanilir
    @POST("uni_etkinlik/{file_name}.php")
    Call<EtkModel> getActivityDetails(@Path("file_name") String file_name);

    //begeni kontrolu icin
    @FormUrlEncoded
    @POST("uni_etkinlik/likeKontrol2.php")
    Call<EtkModel> checkLikeStatus(@Field("email") String email);


    //katılım kontrolü
    @FormUrlEncoded
    @POST("uni_etkinlik/katilimKontrol.php")
    Call<EtkModel> checkKatilimStatus(@Field("email") String email);




    //begeni guncellemesi icin kullanilir
    @FormUrlEncoded
    @POST("uni_etkinlik/likeGuncelle.php")
    Call<Etkinlik> likeArttir(@Field("etk_id") String etk_id,
                              @Field("email") String email,
                              @Field("etk_like") String etk_like);

    //katilim durumu icin kullanilir
    @FormUrlEncoded
    @POST("uni_etkinlik/katilimGuncelle.php")
    Call<Etkinlik> yeniKatilim(@Field("etk_id") String etk_id,
                               @Field("email") String email,
                               @Field("etk_katilimci_sayisi") String etk_katilimci_sayisi);
}
