package com.example.muhammet.unietkinlik;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhammet.unietkinlik.activity.ActivityScreen;
import com.example.muhammet.unietkinlik.fragments.ProfileFragment;
import com.example.muhammet.unietkinlik.models.EtkModel;

import java.util.HashMap;
import java.util.List;

public class EtkinlikAdapter extends BaseAdapter {
    List<EtkModel.EtkModelData> etkinlik;
    Context context;
    ActivityScreen actScreen;
    ProfileFragment profile;


    public EtkinlikAdapter(Context context, List<EtkModel.EtkModelData> etkinlik) {
        this.etkinlik = etkinlik;
        this.context = context;
    }

    @Override
    public int getCount() {
        return etkinlik.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    HashMap<Integer, View> hashMap = new HashMap<Integer, View>();





    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (hashMap.containsKey(i)) {
            return hashMap.get(i);
        }
        view = LayoutInflater.from(context).inflate(R.layout.item_etkinlik_listview, null, false);
        final ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);
        hashMap.put(i,view);


        holder.etk_id.setText(etkinlik.get(i).getEtk_id());
        holder.etk_adi.setText(etkinlik.get(i).getEtk_adi());
        holder.etk_tarih_saat.setText(etkinlik.get(i).getEtk_tarih_saat());
        holder.etk_katilimci_sayisi.setText(etkinlik.get(i).getEtk_katilimci_sayisi());
        holder.etk_like.setText(etkinlik.get(i).getEtk_like());
        holder.etk_begeni_durumu.setText(etkinlik.get(i).getBegeniDurumu());
        holder.etk_link.setText(etkinlik.get(i).getEtk_link());
        holder.katilBtn.setText(etkinlik.get(i).getKatilimDurumu());
        holder.etk_uni_id.setText(etkinlik.get(i).getEtk_uni().get(0).getEtk_uni_id());
        holder.etk_uni.setText(etkinlik.get(i).getEtk_uni().get(0).getEtk_uni());
        holder.etk_lat.setText(etkinlik.get(i).getEtk_uni().get(0).getEtk_lat());
        holder.etk_long.setText(etkinlik.get(i).getEtk_uni().get(0).getEtk_long());

        //Toast.makeText(context, holder.etk_uni.getText().toString(), Toast.LENGTH_SHORT).show();
        if(holder.etk_uni_id.getText().toString().equals("1"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.itu);
        }else if(holder.etk_uni_id.getText().toString().equals("0"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.boun);
        }else if(holder.etk_uni_id.getText().toString().equals("3"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.ytu);
        }else if(holder.etk_uni_id.getText().toString().equals("2"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.med);
        }else if(holder.etk_uni_id.getText().toString().equals("4"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.mu);
        }else {
            holder.etk_uni_logo.setImageResource(R.drawable.error);
        }

        actScreen = new ActivityScreen();




        /*
        holder.pickerButon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String tittle = "Üniversitelerden Etkinlikler";
                String subject = "Yaklaşan bir etkinliğiniz var";
                String body = holder.etk_adi.getText().toString();

                NotificationManager notif = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                Notification notify = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    notify = new Notification.Builder
                            (context.getApplicationContext()).setContentTitle(tittle).setContentText(body).
                            setContentTitle(subject).setSmallIcon(R.drawable.logo1).build();
                }

                notify.flags |= Notification.FLAG_AUTO_CANCEL;
                notif.notify(0, notify);
            }
        });

*/

        holder.etkLikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int begeni = 0;
                //Toast.makeText(ActivityScreen.this, "id = " + etk_id.getText().toString() + "email = " +profile.user_email.toString(), Toast.LENGTH_SHORT).show();
                try{
                    begeni = Integer.parseInt(holder.etk_like.getText().toString());
                }catch (NumberFormatException e){
                    Log.e("Could not parse",e.toString());
                }



                if(holder.etk_begeni_durumu.getText().toString().equals("")){

                    begeni++;
                    holder.etk_like.setText(String.valueOf(begeni));

                    //Toast.makeText(context, holder.etk_id.getText().toString(), Toast.LENGTH_SHORT).show();
                    actScreen.likeGuncelle(holder.etk_id.getText().toString(),profile.user_email.toString(),holder.etk_like.getText().toString());

                    holder.etk_begeni_durumu.setText("Beğendin");
                    holder.etkLikeBtn.setImageResource(R.mipmap.dislike);

                }else if(holder.etk_begeni_durumu.getText().equals("Beğendin")){
                    if(begeni>0)
                        begeni--;
                    else
                        begeni=0;
                    holder.etk_like.setText(String.valueOf(begeni));

                    //Toast.makeText(context, holder.etk_id.getText().toString(), Toast.LENGTH_SHORT).show();
                    actScreen.likeGuncelle(holder.etk_id.getText().toString(),profile.user_email.toString(),holder.etk_like.getText().toString());


                    holder.etk_begeni_durumu.setText("");
                    holder.etkLikeBtn.setImageResource(R.mipmap.like);
                }
            }
        });


        holder.katilBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int katilim = 0;
                //Toast.makeText(ActivityScreen.this, "id = " + etk_id.getText().toString() + "email = " +profile.user_email.toString(), Toast.LENGTH_SHORT).show();
                try{
                    katilim = Integer.parseInt(holder.etk_katilimci_sayisi.getText().toString());
                }catch (NumberFormatException e){
                    Log.e("Could not parse",e.toString());
                }


                if(holder.katilBtn.getText().equals("KATIL")){
                    katilim++;
                    holder.etk_katilimci_sayisi.setText(String.valueOf(katilim));

                    actScreen.katil(holder.etk_id.getText().toString(),profile.user_email.toString(),holder.etk_katilimci_sayisi.getText().toString());

                    holder.katilBtn.setText("KATILMAKTAN VAZGEÇ");
                }else if(holder.katilBtn.getText().equals("KATILMAKTAN VAZGEÇ")){
                    if(katilim>0)
                        katilim--;
                    else
                        katilim=0;
                    holder.etk_katilimci_sayisi.setText(String.valueOf(katilim));

                    actScreen.katil(holder.etk_id.getText().toString(),profile.user_email.toString(),holder.etk_katilimci_sayisi.getText().toString());

                    holder.katilBtn.setText("KATIL");
                }
            }
        });
        holder.etk_adi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(holder.etk_link.getText().toString()));
                context.startActivity(browser);
            }
        });

        holder.etk_konum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent navigation = new Intent(Intent.ACTION_VIEW, Uri
                        .parse("http://maps.google.com/maps?saddr="
                                + actScreen.latitude + ","
                                + actScreen.longitude + "&daddr="
                                + holder.etk_lat.getText() + "," + holder.etk_long.getText()));
                navigation.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                context.startActivity(navigation);
            }
        });



        return view;
    }
/*
    ArrayList<EtkModel.EtkModelData> begeni;
    public String begeniKontrol(final String etk_id, String email){
        // showpDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //Toast.makeText(ActivityScreen.this, s.toString(), Toast.LENGTH_SHORT).show();
        RequestInterface service = retrofit.create(RequestInterface.class);
        final Etkinlik etk = new Etkinlik();
        User user = new User();
        etk.setEtk_id(etk_id.toString());
        user.setEmail(email.toString());
        Log.d("sdasd",etk_id.toString());
        Call<EtkModel> call = service.checkLike(etk.getEtk_id(),user.getEmail());
        call.enqueue(new Callback<EtkModel>() {
            @Override
            public void onResponse(Call<EtkModel> call, Response<EtkModel> response) {
                Log.d("onResponse","Bu etkinlik bu kullanıcı tarafından beğenilmiş      "+response.isSuccessful());
                //Log.d("",response.body().getEtk_adi());

                EtkModel jsonResponse = response.body();
                begeni = new ArrayList<>(Arrays.asList(jsonResponse.getData()));
                for(int i=0; i<begeni.size();i++){
                    out = begeni.get(i).getEtk_id();
                    Log.d("OUT","ID:"+out);
                }

                //hidepDialog();
                //Toast.makeText(ActivityScreen.this, response.body(), Toast.LENGTH_SHORT).show();
                // return String.valueOf(response.body().getEtk_adi());

            }

            @Override
            public void onFailure(Call<EtkModel> call, Throwable t) {
                //hidepDialog();
                Log.d("onFailure","Bu etkinlik bu kullanıcı tarafından beğenilmemiş    "+t.getMessage());

            }
        });
        return out;
    }

*/

    public static class ViewHolder {
        public ImageButton etkLikeBtn;
        public Button katilBtn, pickerButon;
        public TextView etk_adi, etk_begeni_durumu,etk_katilim_durumu, etk_uni,etk_uni_id, etk_tarih_saat,etk_link, etk_katilimci_sayisi, etk_like, tarih, etk_lat, etk_long, etk_id;
        public ImageView etk_uni_logo, etk_konum;

        public ViewHolder(View convertView) {

            this.etk_adi = (TextView) convertView.findViewById(R.id.item_etkinlik_etk_name_text);
            this.etk_tarih_saat = (TextView) convertView.findViewById(R.id.item_etkinlik_etk_tarih_text);
            this.etk_katilimci_sayisi = (TextView) convertView.findViewById(R.id.item_etkinlik_etk_katilimci_sayisi_text);
            this.etk_like = (TextView)  convertView.findViewById(R.id.item_etkinlik_etk_like_text);
            this.etk_link = (TextView)  convertView.findViewById(R.id.item_etkinlik_etk_link_text);
            this.katilBtn = (Button)convertView.findViewById(R.id.item_etkinlik_etk_katil_buton);
            this.etkLikeBtn = (ImageButton) convertView.findViewById(R.id.item_etkinlik_etk_like_buton);
            this.etk_uni_id = (TextView) convertView.findViewById(R.id.item_etkinlik_uni_id_text_view);
            this.etk_uni = (TextView)convertView.findViewById(R.id.item_etkinlik_uni_name_text_view);
            this.etk_uni_logo = (ImageView) convertView.findViewById(R.id.item_etkinlik_etk_logo_image);
            this.etk_konum = (ImageView) convertView.findViewById(R.id.item_etkinlik_etk_konum_image);
            this.etk_id = (TextView)convertView.findViewById(R.id.item_etkinlik_id_text_view);
            this.etk_lat = (TextView)convertView.findViewById(R.id.item_etkinlik_lat_text_view);
            this.etk_long = (TextView)convertView.findViewById(R.id.item_etkinlik_long_text_view);
            this.etk_begeni_durumu = (TextView) convertView.findViewById(R.id.item_etkinlik_etk_like_status);

        }
    }

}

