package com.example.muhammet.unietkinlik;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhammet.unietkinlik.models.EtkModel;

import java.util.List;

/**
 * Created by muhammet on 9.06.2017.
 */

public class AllEtkinlikAdapter extends BaseAdapter{
    List<EtkModel.EtkModelData> etkinlik;
    Context context;

    public AllEtkinlikAdapter(Context context, List<EtkModel.EtkModelData> etkinlik) {
        this.etkinlik = etkinlik;
        this.context = context;
    }

    @Override
    public int getCount() {
        return etkinlik.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.item_etkinlik_listview_all, null, false);
        final AllEtkinlikAdapter.ViewHolder holder = new AllEtkinlikAdapter.ViewHolder(view);

/*
        holder.etk_id.setText(etkinlik.getEtk_id());
        holder.etk_adi.setText(etkinlik.getEtk_adi());
        holder.etk_tarih_saat.setText(etkinlik.getEtk_tarih_saat());
        holder.etk_katilimci_sayisi.setText(etkinlik.getEtk_katilimci_sayisi());
        holder.etk_like.setText(etkinlik.getEtk_like());
        holder.etk_dislike.setText(etkinlik.getEtk_dislike());
        holder.etk_link.setText(etkinlik.getEtk_link());
        holder.etk_uni.setText(etkinlik.getEtk_uni().getEtk_uni());
        holder.etk_lat.setText(etkinlik.getEtk_uni().getEtk_lat());
        holder.etk_long.setText(etkinlik.getEtk_uni().getEtk_long());
      */

        //holder.etk_id.setText(etkinlik.get(i).getEtk_id());
        holder.etk_adi.setText(etkinlik.get(i).getEtk_adi());
        holder.etk_tarih_saat.setText(etkinlik.get(i).getEtk_tarih_saat());
        holder.etk_katilimci_sayisi.setText(etkinlik.get(i).getEtk_katilimci_sayisi());
        holder.etk_like.setText(etkinlik.get(i).getEtk_like());

        //holder.etk_link.setText(etkinlik.get(i).getEtk_link());
        /*
        holder.etk_uni.setText(etkinlik.get(i).getEtk_uni());
        holder.etk_lat.setText(etkinlik.get(i).getEtk_lat());
        holder.etk_long.setText(etkinlik.get(i).getEtk_long());
*/
        holder.etk_uni_id.setText(etkinlik.get(i).getEtk_uni().get(0).getEtk_uni_id());
        holder.etk_uni.setText(etkinlik.get(i).getEtk_uni().get(0).getEtk_uni());
        //holder.etk_lat.setText(etkinlik.get(i).getEtk_uni().get(0).getEtk_lat());
        //holder.etk_long.setText(etkinlik.get(i).getEtk_uni().get(0).getEtk_long());

        //Toast.makeText(context, holder.etk_uni.getText().toString(), Toast.LENGTH_SHORT).show();
        if(holder.etk_uni_id.getText().toString().equals("1"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.itu);
        }else if(holder.etk_uni_id.getText().toString().equals("0"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.boun);
        }else if(holder.etk_uni_id.getText().toString().equals("3"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.ytu);
        }else if(holder.etk_uni_id.getText().toString().equals("2"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.med);
        }else if(holder.etk_uni_id.getText().toString().equals("4"))
        {
            holder.etk_uni_logo.setImageResource(R.drawable.mu);
        }else {
            holder.etk_uni_logo.setImageResource(R.drawable.error);
        }


        return view;
    }



    public static class ViewHolder {

        public ImageButton etkLikeBtn, etkDislikeBtn;
        public Button katilBtn;
        public TextView etk_adi, etk_begeni_durumu,etk_katilim_durumu, etk_uni,etk_uni_id, etk_tarih_saat,etk_link, etk_katilimci_sayisi, etk_like, tarih, etk_lat, etk_long, etk_id;
        public ImageView etk_uni_logo, etk_konum;

        public ViewHolder(View convertView) {

            this.etk_adi = (TextView) convertView.findViewById(R.id.item_etkinlik_etk_name_textAll);
            this.etk_tarih_saat = (TextView) convertView.findViewById(R.id.item_etkinlik_etk_tarih_textAll);
            this.etk_katilimci_sayisi = (TextView) convertView.findViewById(R.id.item_etkinlik_etk_katilimci_sayisi_textAll);
            this.etk_like = (TextView)  convertView.findViewById(R.id.item_etkinlik_etk_like_textAll);
            this.etkLikeBtn = (ImageButton) convertView.findViewById(R.id.item_etkinlik_etk_like_buton);
            this.etk_uni_id = (TextView) convertView.findViewById(R.id.item_etkinlik_uni_id_text_viewAll);
            this.etk_uni = (TextView)convertView.findViewById(R.id.item_etkinlik_uni_name_text_viewAll);
            this.etk_uni_logo = (ImageView) convertView.findViewById(R.id.item_etkinlik_etk_logo_imageAll);
            this.etk_id = (TextView)convertView.findViewById(R.id.item_etkinlik_id_text_viewAll);
            this.etk_begeni_durumu = (TextView) convertView.findViewById(R.id.item_etkinlik_etk_like_statusAll);
            this.etk_katilim_durumu = (TextView) convertView.findViewById(R.id.item_etkinlik_etk_katilim_statusAll);
        }
    }



    }
