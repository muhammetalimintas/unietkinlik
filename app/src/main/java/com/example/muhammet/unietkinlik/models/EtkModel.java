package com.example.muhammet.unietkinlik.models;

/**
 * Created by muhammet on 6.06.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class EtkModel {


    @SerializedName("etkinlik")
    private EtkModelData[] data;

    public EtkModelData[] getData() {
        return data;
    }

    public void setData(EtkModelData[] data) {
        this.data = data;
    }

    public static class EtkModelData {


            @SerializedName("etk_id")
            private String etk_id;

        @SerializedName("etk_adi")
        private String etk_adi;
        @SerializedName("etk_tarih_saat")
        private String etk_tarih_saat;
        @SerializedName("etk_katilimci_sayisi")
        private String etk_katilimci_sayisi;
        @SerializedName("etk_like")
        private String etk_like;

        @SerializedName("etk_link")
        private String etk_link;
        private String begeniDurumu;
        private String katilimDurumu;

        public String getKatilimDurumu() {
            return katilimDurumu;
        }

        public void setKatilimDurumu(String katilimDurumu) {
            this.katilimDurumu = katilimDurumu;
        }

        public String getBegeniDurumu() {
            return begeniDurumu;
        }

        public void setBegeniDurumu(String begeniDurumu) {
            this.begeniDurumu = begeniDurumu;
        }


        @SerializedName("etk_uni")
        private List<EtkUni> etk_uni;

        public String getEtk_id() {
            return etk_id;
        }

        public void setEtk_id(String etk_id) {
            this.etk_id = etk_id;
        }

        public String getEtk_adi() {
            return etk_adi;
        }

        public void setEtk_adi(String etk_adi) {
            this.etk_adi = etk_adi;
        }

        public String getEtk_tarih_saat() {
            return etk_tarih_saat;
        }

        public void setEtk_tarih_saat(String etk_tarih_saat) {
            this.etk_tarih_saat = etk_tarih_saat;
        }

        public String getEtk_katilimci_sayisi() {
            return etk_katilimci_sayisi;
        }

        public void setEtk_katilimci_sayisi(String etk_katilimci_sayisi) {
            this.etk_katilimci_sayisi = etk_katilimci_sayisi;
        }

        public String getEtk_like() {
            return etk_like;
        }

        public void setEtk_like(String etk_like) {
            this.etk_like = etk_like;
        }


        public String getEtk_link() {
            return etk_link;
        }

        public void setEtk_link(String etk_link) {
            this.etk_link = etk_link;
        }

        public List<EtkUni> getEtk_uni() {
            return etk_uni;
        }

        public void setEtk_uni(List<EtkUni> etk_uni) {
            this.etk_uni = etk_uni;
        }

    }

    public static class EtkUni{
        @SerializedName("uni_id")
        @Expose
        public String etk_uni_id;
        @SerializedName("uni_adi")
        @Expose
        public String etk_uni;
        @SerializedName("latitude")
        @Expose
        public String etk_lat;
        @SerializedName("longitude")
        @Expose
        public String etk_long;


        public String getEtk_uni_id() {
            return etk_uni_id;
        }

        public void setEtk_uni_id(String etk_uni_id) {
            this.etk_uni_id = etk_uni_id;
        }

        public String getEtk_uni() {
            return etk_uni;
        }

        public void setEtk_uni(String etk_uni) {
            this.etk_uni = etk_uni;
        }

        public String getEtk_lat() {
            return etk_lat;
        }

        public void setEtk_lat(String etk_lat) {
            this.etk_lat = etk_lat;
        }

        public String getEtk_long() {
            return etk_long;
        }

        public void setEtk_long(String etk_long) {
            this.etk_long = etk_long;
        }
    }

}

