package com.example.muhammet.unietkinlik.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import com.example.muhammet.unietkinlik.AllEtkinlikAdapter;
import com.example.muhammet.unietkinlik.Constants;
import com.example.muhammet.unietkinlik.R;
import com.example.muhammet.unietkinlik.RequestInterface;
import com.example.muhammet.unietkinlik.fragments.ProfileFragment;
import com.example.muhammet.unietkinlik.models.EtkModel;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by muhammet on 29.05.2017.
 */

public class AllActivities extends AppCompatActivity {

    private ListView listViewAll;
    private static ProgressDialog pDialog;
    ProfileFragment profile;
    private ArrayList<EtkModel.EtkModelData> data;
    public String tumu = "get_all_events";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_activities);

        ActionBar ab = getSupportActionBar();
        //ab.hide();

        pDialog = new ProgressDialog(AllActivities.this);
        pDialog.setMessage("Lütfen Bekleyiniz...");
        pDialog.setCancelable(false);

        listViewAll = (ListView) findViewById(R.id.act_main_etkinlikLVAll);
        profile = new ProfileFragment();
        getJsonRetrofitArray(tumu);

    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void getJsonRetrofitArray(String choose) {


        showpDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL).//ip
                addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface service = retrofit.create(RequestInterface.class);
        Call<EtkModel> modelCall = service.getActivityDetails(choose);

        modelCall.enqueue(new Callback<EtkModel>() {
            @Override
            public void onResponse(Call<EtkModel> call, Response<EtkModel> response) {
                hidepDialog();
                try {
                    //EtkModel model  = response.body();
                    EtkModel jsonResponse = response.body();
                    data = new ArrayList<>(Arrays.asList(jsonResponse.getData()));

                    //EtkModel.EtkModelData[] userList  = model.getData();


                    if (data == null) {
                        Toast.makeText(AllActivities.this, "Kayıtı Veri Yok", Toast.LENGTH_SHORT).show();
                    } else {
                        AllEtkinlikAdapter adapter = new AllEtkinlikAdapter(AllActivities.this,data);
                        listViewAll.setAdapter(adapter);
                        //Toast.makeText(ActivityScreen.this, "SIKINTI YOK", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    Toast.makeText(AllActivities.this, "Bu seçenek için veritabanında kayıt bulunamadı. Tarihe göre sıralanıyor. ", Toast.LENGTH_LONG).show();
                    getJsonRetrofitArray(tumu);
                }
            }

            @Override
            public void onFailure(Call<EtkModel> call, Throwable t) {
                hidepDialog();
                Toast.makeText(AllActivities.this, "Lütfen Daha Sonra Tekrar Deneyiniz" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

    }

}
